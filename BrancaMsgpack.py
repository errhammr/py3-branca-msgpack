#!/usr/bin/env python3

# Dependencies:
# libsodium
# pybranca (pip)
# msgpack (pip)

from branca import Branca
import msgpack

class BrancaMsgpack:
	
	def createToken(self, payload):
		b = Branca(key=self.key32)
		packed = msgpack.dumps(payload)
		token = b.encode(packed)
		return token
	
	def parseToken(self, token, expires=None):
		b = Branca(key=self.key32)
		rawPayload = b.decode(token, ttl=expires)
		payload = msgpack.loads(rawPayload, raw=False)
		return payload
	
	def __init__(self, key32):
		self.key32 = key32

if __name__ == "__main__":
	secret = "OaHqMhoiWm7NoMHZcM6ub2c94su1vtEX"
	t = BrancaMsgpack(secret)
	token = t.createToken([42, 1337])
	print("token:", token)
	payload = t.parseToken(token)
	print("payload:", payload)
	print("raw payload:", msgpack.dumps(payload))
	
	print("Testing an expired token...")
	try:
		expiredToken = "4DWnY59J05q0HWVxdmLwS1a6H4oi0WKoJshrotnCxHk8rndYB6I6s3O8LNzEcF8eEcvLkaQKoU3LsG2sBW8YDeCrzNUNxuJHMjV0"
		expiredPayload = t.parseToken(expiredToken, expires=60) # this token expires after 60 seconds
	except RuntimeError as e:
		print("Error:", e)
 