# py3-branca-msgpack

Encode any value in a Branca token using msgpack

See [pybranca](https://github.com/tuupola/pybranca) and [Branca spec](https://github.com/tuupola/branca-spec/)